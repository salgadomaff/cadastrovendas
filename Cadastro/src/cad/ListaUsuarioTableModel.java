
package cad;

/**
 *
 * @author ksandim
 */




import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;


      
    
    public class ListaUsuarioTableModel extends AbstractTableModel {  
        
        
      
        /* Lista de recibos que representam as linhas. */  
        private List<Usuario> linhas;  
      
        /* Array de Strings com o nome das colunas. */  
        private String[] colunas = new String[] {  
                "ID", "Nome"};  
      
      
        /* Cria um ReciboTableModel vazio. */  
        public ListaUsuarioTableModel() {  
            linhas = new ArrayList<Usuario>();  
        }
             
        /* Cria um ReciboTableModel carregado com 
         * a lista de recibos especificada. */  
        public ListaUsuarioTableModel(List<Usuario> a) {  
            linhas = new ArrayList<Usuario>(a);  
        }  
      
      
        /* Retorna a quantidade de colunas. */  
        @Override  
        public int getColumnCount() {  
            // Está retornando o tamanho do array "colunas".  
            // Mas como o array é fixo, vai sempre retornar 4.  
            return colunas.length;  
        }  
      
        /* Retorna a quantidade de linhas. */  
        @Override  
        public int getRowCount() {  
            // Retorna o tamanho da lista de recibos.  
            return linhas.size();  
        }  
      
        /*
         * Retorna o nome da coluna no índice especificado. Este método é usado
         * pela JTable para saber o texto do cabeçalho.
         */
        @Override
        public String getColumnName(int columnIndex) {
            // Retorna o conteúdo do Array que possui o nome das colunas  
            // no índice especificado.  
            return colunas[columnIndex];
        };  
      
        /* Retorna a classe dos elementos da coluna especificada. 
         * Este método é usado pela JTable na hora de definir o editor da célula. */  
        @Override
        public Class<?> getColumnClass(int columnIndex) {
            // Retorna a classe referente a coluna especificada.  
            // Aqui é feito um switch para verificar qual é a coluna  
            // e retornar o tipo adequado. As colunas são as mesmas  
            // que foram especificadas no array "colunas".  
            switch (columnIndex) {

                case 0: // ID  
                    return int.class;

                case 1: //nome
                    return String.class;

                default:
                    // Se o índice da coluna não for válido, lança um  
                    // IndexOutOfBoundsException (Exceção de índice fora dos limites).  
                    // Não foi necessário verificar se o índice da linha é inválido,  
                    // pois o próprio ArrayList lança a exceção caso seja inválido.  
                    throw new IndexOutOfBoundsException("columnIndex out of bounds");
            }
        }
      
        /* Retorna o valor da célula especificada 
         * pelos índices da linha e da coluna. */  
        @Override  
        public Object getValueAt(int rowIndex, int columnIndex) {  
            // Pega o recibo da linha especificada.  
            Usuario c = linhas.get(rowIndex);

            // Retorna o campo referente a coluna especificada.  
            // Aqui é feito um switch para verificar qual é a coluna  
            // e retornar o campo adequado. As colunas são as mesmas  
            // que foram especificadas no array "colunas".  
            switch (columnIndex) {
                case 0: //ID  
                    return  c.getId();
                case 1: //Nome
                    return c.getNome();              
                    
                default:
                    // Se o índice da coluna não for válido, lança um  
                    // IndexOutOfBoundsException (Exceção de índice fora dos limites).  
                    // Não foi necessário verificar se o índice da linha é inválido,  
                    // pois o próprio ArrayList lança a exceção caso seja inválido.  
                    throw new IndexOutOfBoundsException("columnIndex out of bounds");
            }
        }
        
        
        
      
        /* Seta o valor da célula especificada 
         * pelos índices da linha e da coluna. 
         * Aqui ele está implementado para não fazer nada, 
         * até porque este table model não é editável. */  
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {};  
      
        /* Retorna um valor booleano que define se a célula em questão 
         * pode ser editada ou não. 
         * Este método é utilizado pela JTable na hora de definir o editor da célula. 
         * Neste caso, estará sempre retornando false, não permitindo que nenhuma 
         * célula seja editada. */  
        @Override  
        public boolean isCellEditable(int rowIndex, int columnIndex) {  
            
            boolean value = false;
            
            if(columnIndex == 0){
                value = true;
            }
            
            return value;  
        }
              
        ////////////////////////////////////////////////////////////  
        // Os métodos declarados até aqui foram as implementações //  
        // de TableModel, que são continuamente utilizados        //  
        // pela JTable para definir seu comportamento,            //  
        // por isso o nome Table Model (Modelo da Tabela).        //  
        //                                                        //  
        // A partir de agora, os métodos criados serão            //  
        // particulares desta classe.                             //  
        ////////////////////////////////////////////////////////////  
      
      
        /* Retorna o recibo da linha especificada. */  
        public Usuario getAgenda(int indiceLinha) {  
            return linhas.get(indiceLinha);  
        }  
          
        /* Adiciona um registro. */  
        public void addUsuario(Usuario usuario) {  
            // Adiciona o registro.  
            linhas.add(usuario);  
      
            // Pega a quantidade de registros e subtrai um para achar  
            // o último índice. É preciso subtrair um, pois os índices  
            // começam pelo zero.  
            int ultimoIndice = getRowCount() - 1;  
      
            // Reporta a mudança. O JTable recebe a notificação  
            // e se redesenha permitindo que visualizemos a atualização.  
            fireTableRowsInserted(ultimoIndice, ultimoIndice);  
        }  
      
        /* Remove a linha especificada. */  
        public void removeCliente(int indiceLinha) {  
            // Remove o recibo da linha especificada.          
            linhas.remove(indiceLinha);  
      
            // Reporta a mudança. O JTable recebe a notificação  
            // e se redesenha permitindo que visualizemos a atualização.  
            fireTableRowsDeleted(indiceLinha, indiceLinha);  
        }  
      
        /* Adiciona uma lista de recibos ao final dos registros. */  
        public void addListaMorador(List<Usuario> usuario) {  
            // Pega o tamanho antigo da tabela.  
            int tamanhoAntigo = getRowCount();  
      
            // Adiciona os registros.  
            linhas.addAll(usuario);  
      
            // Reporta a mudança. O JTable recebe a notificação  
            // e se redesenha permitindo que visualizemos a atualização.  
            fireTableRowsInserted(tamanhoAntigo, getRowCount() - 1);  
        }  
      
        /* Remove todos os registros. */  
        public void limpar() {  
            // Remove todos os elementos da lista de recibos.  
            linhas.clear();  
      
            // Reporta a mudança. O JTable recebe a notificação  
            // e se redesenha permitindo que visualizemos a atualização.  
            fireTableDataChanged();  
        }  
      
        /* Verifica se este table model está vazio. */  
        public boolean isEmpty() {  
            return linhas.isEmpty();  
        }  
      
    }  


